<%@ WebHandler Language="C#" Class="GetRequestStatusHistory" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MarvalSoftware.Data;
using MarvalSoftware.Data.ServiceDesk;
using MarvalSoftware.Processes;
using MarvalSoftware.Security;


/// <summary>
/// ApiHandler
/// </summary>
public class GetRequestStatusHistory : IHttpHandler
{

    #region Properties

    /// <summary>
    /// Gets or sets whether or not this handler is reusable.
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Methods

    public void ProcessRequest(HttpContext context)
    {
        int requestId = int.Parse(context.Request.QueryString["requestId"]);

        // set the response content type
        context.Response.ContentType = "application/json";
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        WorkflowBroker workflowBroker = new WorkflowBroker();
        UserBroker userBroker = new UserBroker();
        List<InternalRequestStatusHistory> requestStatusHistory = new List<InternalRequestStatusHistory>();
        using (DataGrunt grunt = new DataGrunt())
        {
            using (IDataReader reader = grunt.ExecuteReader("requestStatus_getHistory", new DataGrunt.DataGruntParameter("requestId", requestId)))
            {
                while (reader.Read())
                {
                    InternalRequestStatusHistory history = new InternalRequestStatusHistory
                    {
                        StatusName = new StatusBroker().Find(reader.GetInt32(reader.GetOrdinal("statusId"))).Name,
                        StartDate = reader.GetDateTime(reader.GetOrdinal("startDate")).ToLocalTime().ToString("yyyy-MM-dd HH:mm"),
                        Workflow = workflowBroker.FindProcessWorkflow(reader.GetInt32(reader.GetOrdinal("workflowId"))).Name,
                        UpdatedBy = userBroker.Find(reader.GetInt32(reader.GetOrdinal("createdByUserId"))).NameString,
                    };

                    requestStatusHistory.Add(history);
                }
            }
        }

        string json = oSerializer.Serialize(requestStatusHistory);
        context.Response.Write(json);
    }

    #endregion
}

public class InternalRequestStatusHistory
{
    public String StatusName  { get; set; }
    public String StartDate  { get; set; }
    public String Workflow { get; set; }
    public String UpdatedBy { get; set; }
}
    