# Plugin to Show Status History

## Information


## Installation

	Please see your MSM documentation for information on how to install plugins.

## Dependence of MSM code

	- _workflowStatusPicker.isEnabled()
	- _workflowStatusPicker._isNextStateSolveCloseHavingUnsolvedSubRequest

## Functions

### 0.1
	
	- Show request status history

## Compatible Versions

| Plugin   | MSM         |
|----------|-------------|
| 0.1	   | 14.10       |